package login;

import base.BaseTest;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import tasks.isLoginMenuInicio;
import tasks.IsLoginErrorMessage;
import tasks.Login;

import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class LoginTests extends BaseTest {

    @Test
    public void testSuccessfulLogin(){

        Login.as(webDriver,"lailacastillo111@gmail.com","testingbolvia123456");
        String pageURL = webDriver.getCurrentUrl();

        Assert.assertEquals(pageURL,"https://users.wix.com/signin?forceRender=true");
        if (pageURL.equals("https://users.wix.com/signin?forceRender=true")){
            System.out.println(pageURL + " Solicita verificar que no es un robot!");
        }
        if (pageURL.contains("https://users.wix.com/dashboard")){
            System.out.println(pageURL + " Logeado correctamente!");
        }

        //Assert.assertTrue(isLoginMenuInicio.visible(webDriver));
        //Assert.assertEquals(pageURL, "https://manage.wix.com/dashboard/9fa254c9-3b1c-452c-876e-0ea523ad0383/home");
        //webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //webDriver.navigate().to("https://manage.wix.com/dashboard/9fa254c9-3b1c-452c-876e-0ea523ad0383/home");



    }

    @Test
    public void testLoginWithInvalidCredentials(){
        Login.as(webDriver,"admin","admin");
        Assert.assertTrue(IsLoginErrorMessage.visible(webDriver));
    }
}
