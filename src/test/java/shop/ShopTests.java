package shop;

import base.BaseShopTest;
import base.BaseTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import tasks.Login;
import tasks.Micromarket;

import java.util.concurrent.TimeUnit;

public class ShopTests extends BaseShopTest {

    @Test
    public void testA_goShop(){
        Assert.assertEquals(webDriver.getCurrentUrl(),"https://lailacastillo111.wixsite.com/micromarket/");
        if (webDriver.getCurrentUrl().contains("ttps://lailacastillo111.wixsite.com/micromarket")){
            System.out.println("1.- Navegando correctamente en Micromarket!");
        }


    }

    @Test
    public void testB_golinkComprar(){

        Micromarket.irLinkCompras(webDriver);
        Assert.assertEquals(webDriver.getCurrentUrl(),"https://lailacastillo111.wixsite.com/micromarket/shop");
        System.out.println("2.- Entro a link de Compras!");
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

    }

    @AfterTest
    public void TestC_goElegirZapato(){
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        String producto = "Zapato A";
        Micromarket.eligirProducto(webDriver,producto);
        System.out.println("3.- Eligiendo producto:!" + producto);

        Micromarket.clickRadioButton(webDriver);
        System.out.println("3.1.- Tickeando opcion");

        System.out.println("Current Url:"+ webDriver.getCurrentUrl());
        Micromarket.agregarCarrito(webDriver);
        System.out.println("4.- Agregando al carrito el Producto:!" + producto);


       /* Micromarket.verPopUpCarrito(webDriver);
        System.out.println("5.- Ver PopUp carrito de Compras!");

        Micromarket.verCarrito(webDriver);
        System.out.println("6.- Ver carrito de Compras!");*/

    }

}


