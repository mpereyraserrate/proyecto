package base;

import com.aventstack.extentreports.Status;
import helper.ScreenShotHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;
import report.ReportManager;

public class BaseShopTest {
    protected WebDriver webDriver;


    @BeforeSuite
    public static void setUpSuite() throws Exception {
        ReportManager.init("C:\\Reports", "LoginSite");
    }

    @BeforeMethod
    public void setUp(ITestResult iTestResult){

        ReportManager.getInstance().startTest(iTestResult.getMethod().getMethodName());

        System.setProperty("webdriver.chrome.driver","resource/chromedriver.exe");
        webDriver = new ChromeDriver();
        webDriver.get("https://lailacastillo111.wixsite.com/micromarket/");

    }

    @AfterMethod
    public void tearDown(ITestResult iTestResult){
        try {
            switch (iTestResult.getStatus()){
                case ITestResult.FAILURE:
                    ReportManager.getInstance().getTest().log(Status.FAIL, "Test failed");
                    break;
                case ITestResult.SKIP:
                    ReportManager.getInstance().getTest().log(Status.SKIP, "Test skipped");
                    break;
                case ITestResult.SUCCESS:
                    ReportManager.getInstance().getTest().log(Status.PASS, "Test passed");
                    break;
                default:
                    ReportManager.getInstance().getTest().log(Status.FAIL, "Test incomplete");
            }

            if(iTestResult.getStatus() != ITestResult.SUCCESS && iTestResult.getThrowable() != null){
                ReportManager.getInstance().getTest().log(Status.FAIL, iTestResult.getThrowable().getMessage());
                ScreenShotHelper.takeScreenShotAndAdToHTMLReport(webDriver, Status.FAIL, "Failure Image");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
           // if(webDriver != null){}
           // webDriver.quit();
        }
    }

    @AfterSuite
    public void cerrarWebDriver(){
       // webDriver.quit();
    }

    @AfterSuite
    public static void tearDownSuite(){

        ReportManager.getInstance().flush();
    }
}
