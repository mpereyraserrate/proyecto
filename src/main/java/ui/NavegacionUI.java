package ui;

import org.openqa.selenium.By;

public class NavegacionUI {

    public static By nav = By.id("comp-kvx6g00hnavContainer");
    public static By linkComprar= By.xpath("//*[@id=\"comp-kvx6g00h1\"]/a");
    public static By AgregarCarrito = By.xpath("//button[@data-hook=\"add-to-cart\"]");
    public static By verCarrito = By.xpath("//a[@id=\"widget-view-cart-button\"]");
    public static By popUpCarrito = By.xpath("//div[@class=\"bQgup\"]");
    public static By radioButton = By.xpath("//div[@data-hook=\"color-picker-item\"]");

}
