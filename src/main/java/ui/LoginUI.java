package ui;

import org.openqa.selenium.By;

public class LoginUI {
    public static By userInput = By.name("email");
    public static By passWordInput = By.name("password");
    public static By loginButton = By.name("submit");
    public static By errorMessage = By.xpath("//i[@class=\"login-svg-font-icons-error-new error ng-scope\"]");
    public static By noRobot = By.xpath("/html/body/div[2]/div[3]/div[1]/div/div/span");
}
