package tasks;

import actions.Click;
import actions.Enter;
import org.openqa.selenium.WebDriver;
import ui.ItemsShopUI;
import ui.NavegacionUI;

public class Micromarket {

    private static ItemsShopUI obj;

    public static void irLinkCompras(WebDriver driver){
        Click.on(driver, NavegacionUI.linkComprar);
    }

    public static void eligirProducto(WebDriver driver,String producto){
        obj = new ItemsShopUI(producto);
        Click.on(driver, obj.ItemProducto);
    }

    public static void agregarCarrito(WebDriver driver){
        Click.on(driver,NavegacionUI.AgregarCarrito);
    }

    public static void verCarrito(WebDriver driver){
        Click.on(driver,NavegacionUI.verCarrito);
    }

    public static void verPopUpCarrito(WebDriver driver){
        Click.on(driver,NavegacionUI.popUpCarrito);
    }

    public static void clickRadioButton(WebDriver driver){
        Click.on(driver,NavegacionUI.radioButton);
    }

}
