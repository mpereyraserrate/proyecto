package tasks;

import actions.WaitUntilElement;
import org.openqa.selenium.WebDriver;
import ui.MenuUI;
import ui.LoginUI;

public class isLoginMenuInicio {
    public static boolean visible(WebDriver webDriver){
        return WaitUntilElement.isVisible(webDriver, MenuUI.MenuInicio);
    }
}
